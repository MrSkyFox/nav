//Sidebar
$(document).ready(function () {

   $('#sidebarCollapse').on('click', function () {
       $('#sidebar').toggleClass('active');
       $(this).toggleClass('active');
   });

   function navLinter() {
     var $elemens = $('[data-init="navLinter"]');
     var currentPath = window.location.pathname;

     $elemens.each(function(_, item) {
       $(item).find('a[href^="' + currentPath + '"]').addClass('active');
     });
   }

   navLinter();
});
